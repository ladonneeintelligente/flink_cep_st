name := "atp_stpoc_flink"

version := "0.1"

scalaVersion := "2.12.12"

libraryDependencies ++= Seq("org.apache.flink" %% "flink-scala" % "1.11.2",
  "org.apache.flink" %% "flink-clients" % "1.11.2",
  "org.apache.flink" %% "flink-streaming-scala" % "1.11.2",
  "org.apache.flink" %% "flink-cep" % "1.11.2")
