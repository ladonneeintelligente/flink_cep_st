package com.ldi;

/**
 *https://medium.com/@ebrahim.soroush/a-simple-introduction-to-flink-data-stream-processing-and-its-cep-library-5295755ffb56
 *  * Second Step: Preparing a fictitious IOT device source
 * * The first thing we need is the source of the data stream.
 * Flink can connect to the number of sources including text files, network cat data and etc.
 * (See the official docs here).
 * For simplicity, we’re going to implement a simple source which produces random data streams of IoT sensors.
 * I’ve written a simple TemperatureSensor.java class that inherits from RichSourceFunction to provide the source of data
 * for our Flink stream processing. It only has a deviceId and a temperature value for that device.
 * https://github.com/e3oroush/flink-tutorial
 */


public class Sensor {

    private int deviceId;
    private double temperature;
    private long timestamp;

    public Sensor(int deviceId, double temperature, long timestamp)
    {
        this.deviceId = deviceId;
        this.temperature = temperature;
        this.timestamp = timestamp;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "device id: " + this.deviceId + ", temperature: " + this.temperature;
    }
}
